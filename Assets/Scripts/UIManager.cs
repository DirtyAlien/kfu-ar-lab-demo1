﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance { get; private set; }

    public Text DebugText => debugText;
    public ArticlePanel ArticlePanel => articlePanel;
    public Button MapButton => mapButton;
    public Image TapBlocker => tapBlocker;
    public MapPanel MapPanel => mapPanel;
    public SpotsPanel SpotsPanel => spotsPanel;

    [SerializeField]
    private Text debugText;
    [SerializeField]
    private ArticlePanel articlePanel;
    [SerializeField]
    private Button mapButton;
    [SerializeField]
    private Image tapBlocker;
    [SerializeField]
    private MapPanel mapPanel;
    [SerializeField]
    private SpotsPanel spotsPanel;

    private void Awake() {
        Instance = this;
        Application.targetFrameRate = 60;
    }

    public void Update() {
        if (Application.isEditor) {
            // TODO: REMOVE
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                PlayerPrefs.DeleteAll();
            }
            var deltaLatLon = new Vector2(Input.GetAxisRaw("Vertical"), Input.GetAxisRaw("Horizontal")).normalized * 0.001f * Time.deltaTime;
            if (deltaLatLon != Vector2.zero) {
                LocationService.Instance.MoveDummyPos(deltaLatLon.x, deltaLatLon.y);
            }
        }
    }

    public void OnMapButtonPressed() {
        AudioManager.Instance.PlaySound("chop");
        mapPanel.Show();
    }
}
