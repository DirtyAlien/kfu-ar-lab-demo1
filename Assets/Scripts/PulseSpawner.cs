﻿using System.Collections;
using UnityEngine;

public class PulseSpawner : MonoBehaviour {

    public RectTransform RectTransform { get; private set; }

    [SerializeField]
    private PulseAnimation pulseAnimationPrefab;
    [SerializeField]
    private float pulseAnimationSpawnPeriod = 2f;
    [SerializeField]
    private int pulseAnimationSpawnCount = 1;

    private void Awake() {
        RectTransform = GetComponent<RectTransform>();
    }

    private void Start () {
        StartCoroutine(RadarCircleSpawn());
	}

    private IEnumerator RadarCircleSpawn() {
        while (true) {
            for (int i = 0; i < pulseAnimationSpawnCount; i++) {
                Instantiate(pulseAnimationPrefab, transform);
                yield return new WaitForSeconds(.3f);
            }
            yield return new WaitForSeconds(pulseAnimationSpawnPeriod);
        }
    }
}
