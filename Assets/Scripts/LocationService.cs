﻿using System.Collections;
using UnityEngine;

public class LocationService : MonoBehaviour {

    public static LocationService Instance { get; private set; }

    public event System.Action<State> OnInitializationError;
    public event System.Action<float, float> OnNewLocationInfoAvailable;

    public State CurrentState { get; private set; }
    public float Latitude { get; private set; }
    public float Longitude { get; private set; }
    public double DataOutdateSeconds { get; private set; }

    private double lastDataTimestamp = 0;
    private float lastLatitude = 0;
    private float lastLongitude = 0;

    [SerializeField]
    private float connectionTimeout;
    [SerializeField]
    private float outdatedThreshold;
    [SerializeField]
    private float maximumUpdateRate;

    [SerializeField]
    private float dummyInitTime;
    [SerializeField]
    private State dummyStateAfterInit;
    [SerializeField]
    private float dummyLat;
    [SerializeField]
    private float dummyLong;

    private void Awake() {
        Instance = this;
    }

    private void Update() {
        UIManager.Instance.DebugText.text = "latitude: " + Latitude + 
            "\nlongitude: " + Longitude +
            "\noudated secs: " + ((int)DataOutdateSeconds).ToString() +
            "\nstate: " + CurrentState;
    }

    public void StartService() {
        StopAllCoroutines();
        if (Application.isEditor) {
            StartCoroutine(RunDummyGeoLocService());
        } else {
            StartCoroutine(RunGeoLocService(connectionTimeout));
        }
    }

    public void StopService() {
        StopAllCoroutines();
        Input.location.Stop();
    }

    private IEnumerator RunGeoLocService(float maxWaitInSec) {
        float updateDelay = 1f / maximumUpdateRate;
        var epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        CurrentState = State.Initializing;
        Input.location.Start();
        while (Input.location.status == LocationServiceStatus.Initializing && maxWaitInSec > 0) {
            yield return new WaitForSeconds(.1f);
            maxWaitInSec -= .1f;
        }
        if (maxWaitInSec > 0) {
            if (Input.location.status != LocationServiceStatus.Failed) {
                CurrentState = State.OK;
                while (true) {
                    if (Input.location.lastData.timestamp > lastDataTimestamp) {
                        lastDataTimestamp = Input.location.lastData.timestamp;
                        Latitude = Input.location.lastData.latitude;
                        Longitude = Input.location.lastData.longitude;
                        if (Latitude != lastLatitude || Longitude != lastLongitude) {
                            lastLatitude = Latitude;
                            lastLongitude = Longitude;
                            OnNewLocationInfoAvailable(Latitude, Longitude);
                        }
                    } else {
                        double currentTimestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
                        DataOutdateSeconds = currentTimestamp - Input.location.lastData.timestamp;
                        if (DataOutdateSeconds > outdatedThreshold) {
                            CurrentState = State.OutdatedData;
                            break;
                        }
                    }
                    yield return new WaitForSeconds(updateDelay);
                }
            } else {
                CurrentState = State.ConnectionFailed;
            }
        } else {
            CurrentState = State.TimedOut;
        }
        if (!Input.location.isEnabledByUser) {
            CurrentState = State.BlockedByUser;
        }
        OnInitializationError?.Invoke(CurrentState);
    }

    private IEnumerator RunDummyGeoLocService() {
        print("starting RunDummyGeoLocService...");
        float updateDelay = 1f / maximumUpdateRate;
        CurrentState = State.Initializing;
        yield return new WaitForSeconds(dummyInitTime);
        CurrentState = dummyStateAfterInit;
        if (CurrentState == State.OK) {
            while (true) {
                if (Input.GetKey(KeyCode.Alpha2)) {
                    print("dummy outdated data error processing...");
                    CurrentState = State.OutdatedData;
                    break;
                }
                Latitude = dummyLat;
                Longitude = dummyLong;
                if (Latitude != lastLatitude || Longitude != lastLongitude) {
                    lastLatitude = Latitude;
                    lastLongitude = Longitude;
                    OnNewLocationInfoAvailable(Latitude, Longitude);
                }
                yield return new WaitForSeconds(updateDelay);
            }
        }
        OnInitializationError?.Invoke(CurrentState);
    }

    public void MoveDummyPos(float lat, float lon) {
        dummyLat += lat;
        dummyLong += lon;
    }

    public enum State { Initializing, BlockedByUser, TimedOut, ConnectionFailed, OutdatedData, OK }
}
