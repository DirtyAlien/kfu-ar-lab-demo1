﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

public class ContentManager : MonoBehaviour {

    public static ContentManager Instance { get; private set; }

    public Dictionary<string, SpotData> ContentData { get; private set; }

    [SerializeField]
    private string contentFileName;

    void Awake () {
        Instance = this;
        string json = Resources.Load<TextAsset>(contentFileName).text;
        ContentData = JsonConvert.DeserializeObject<Dictionary<string, SpotData>>(json);
    }
}
