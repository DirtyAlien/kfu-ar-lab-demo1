﻿using UnityEngine;
using UnityEngine.UI;

public class ConnectingPanel : MonoBehaviour {

    [SerializeField]
    private RectTransform contentHolder;
    [SerializeField]
    private Text infoText;
    [SerializeField]    
    private Button retryButton;

    void Start () {
        LocationService.Instance.OnInitializationError += LocationService_OnInitializationError;
        LocationService.Instance.OnNewLocationInfoAvailable += LocationService_OnNewLocationInfoAvailable;
        LocationService.Instance.StartService();
    }

    private void LocationService_OnNewLocationInfoAvailable(float latutude, float longitude) {
        if (contentHolder.gameObject.activeInHierarchy) {
            contentHolder.gameObject.SetActive(false);
            UIManager.Instance.MapButton.gameObject.SetActive(true);
        }
    }

    private void LocationService_OnInitializationError(LocationService.State errorState) {
        AudioManager.Instance.PlaySound("error");
        contentHolder.gameObject.SetActive(true);
        UIManager.Instance.MapButton.gameObject.SetActive(false);
        UIManager.Instance.MapPanel.Hide(false);
        UIManager.Instance.ArticlePanel.Hide(false);
        UIManager.Instance.SpotsPanel.RemoveAllItems();
        if (errorState == LocationService.State.BlockedByUser) {
            infoText.text = "Пожалуйста, разрешите приложению использовать ваше местопложение.";
        } else if (errorState == LocationService.State.TimedOut) {
            infoText.text = "Время ожидания подключения вышло. Проверьте, включен ли GPS.";
        } else if (errorState == LocationService.State.ConnectionFailed) {
            infoText.text = "Произошла ошибка плдключения. Проверьте, включен ли GPS.";
        } else if (errorState == LocationService.State.OutdatedData) {
            infoText.text = "Не удалось получить новые данные о местоположении. Проверьте, включен ли GPS.";
        }
        retryButton.interactable = true;
    }

    public void OnRetryButtonPressed() {
        AudioManager.Instance.PlaySound("click");
        LocationService.Instance.StartService();
        infoText.text = "Подключение к сервису геолокации...";
        retryButton.interactable = false;
    }
}
