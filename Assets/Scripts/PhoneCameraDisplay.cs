﻿using UnityEngine;
using UnityEngine.UI;

public class PhoneCameraDisplay : MonoBehaviour {

    private bool isCameraAvailable = false;
    private WebCamTexture backCamera;
    private int orientationAngle;
    private int lastFrameWidth = 0;
    private int lastFrameHeight = 0;

    [SerializeField]
    private RawImage background;

    private void Start () {
        if (Application.isEditor) {
            background.gameObject.SetActive(false);
        } else {
            Screen.autorotateToLandscapeLeft = false;
            Screen.autorotateToLandscapeRight = false;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;
            CheckDeviceCameras();
        }
    }

    private void CheckDeviceCameras() {
        var devices = WebCamTexture.devices;
        foreach (var device in devices) {
            if (!device.isFrontFacing) {
                backCamera = new WebCamTexture(device.name, Screen.width / 2, Screen.height / 2, 60);
                break;
            }
        }
        if (backCamera == null) {
            isCameraAvailable = false;
            return;
        }
        backCamera.Play();
        background.texture = backCamera;
        isCameraAvailable = true;
    }
	
	private void Update () {
        if (!isCameraAvailable) {
            CheckDeviceCameras();
        } else if (lastFrameWidth != backCamera.width && lastFrameHeight != backCamera.height) {
            orientationAngle = -backCamera.videoRotationAngle;
            if (backCamera.videoVerticallyMirrored) {
                orientationAngle += 180;
            }
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orientationAngle);
            float wr = (float)1080 / backCamera.height;
            float hr = (float)1920 / backCamera.width;
            float r = Mathf.Max(wr, hr);
            background.rectTransform.sizeDelta = new Vector2(backCamera.width * r, backCamera.height * r);

            lastFrameWidth = backCamera.width;
            lastFrameHeight = backCamera.height;
        }
	}
}
