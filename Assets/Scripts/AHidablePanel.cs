﻿using System.Collections;
using UnityEngine;

public abstract class AHidablePanel : MonoBehaviour {

    public bool IsShowed { get; protected set; } = false;

    protected RectTransform rectTransform;
    protected bool isInAnimation;

    protected virtual void Awake() {
        rectTransform = transform as RectTransform;
    }

    protected virtual void Start() {
        rectTransform.anchoredPosition = new Vector2(rectTransform.rect.width, 0);
    }

    protected virtual void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) { //smartphone back button pressed
            Hide();
        }
    }

    public virtual void Show() {
        if (!IsShowed && !isInAnimation) {
            IsShowed = true;
            UIManager.Instance.TapBlocker.gameObject.SetActive(true);
            rectTransform.anchoredPosition = new Vector2(rectTransform.rect.width, 0);
            StartCoroutine(MoveHorizontalAnimation(.15f, 0, () => {
                UIManager.Instance.TapBlocker.gameObject.SetActive(false);
            }));
        }
    }

    public abstract void OnHideCompleted();

    public virtual void Hide(bool playSound = true) {
        if (IsShowed && !isInAnimation) {
            IsShowed = false;
            StartCoroutine(MoveHorizontalAnimation(.15f, rectTransform.rect.width, () => {
                OnHideCompleted();
            }));
            if (playSound) {
                AudioManager.Instance.PlaySound("chop");
            }
        }
    }

    public virtual void HideImmediate() {
        if (IsShowed) {
            IsShowed = false;
            StopAllCoroutines();
            rectTransform.anchoredPosition = new Vector2(rectTransform.rect.width, 0);
            OnHideCompleted();
        }
    }

    protected IEnumerator MoveHorizontalAnimation(float time, float to, System.Action OnAnimationFinish = null) {
        isInAnimation = true;
        float speed = 1f / time;
        float percent = 0;
        float from = rectTransform.anchoredPosition.x;
        while (percent < 1) {
            rectTransform.anchoredPosition = new Vector2(Mathf.Lerp(from, to, percent), 0);
            percent += Time.deltaTime * speed;
            yield return null;
        }
        rectTransform.anchoredPosition = new Vector2(to, 0);
        OnAnimationFinish?.Invoke();
        isInAnimation = false;
    }
}
