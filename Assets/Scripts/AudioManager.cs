﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager: MonoBehaviour {

    public static AudioManager Instance { get; private set; }

    [SerializeField]
    private AudioItem[] audioItems;

    private Dictionary<string, System.Tuple<AudioClip, float>> clipByName;

    private void Awake() {
        clipByName = new Dictionary<string, System.Tuple<AudioClip, float>>();
        foreach (var item in audioItems) {
            clipByName.Add(item.name, new System.Tuple<AudioClip, float>(item.clip, item.volume));
        }
        Instance = this;
    }

    public void PlaySound(string soundName) {
        System.Tuple<AudioClip, float> clipAndVolume;
        if (clipByName.TryGetValue(soundName, out clipAndVolume)) {
            AudioSource.PlayClipAtPoint(clipAndVolume.Item1, Vector3.zero, clipAndVolume.Item2);
        }
    }

    [System.Serializable]
    public struct AudioItem {
        public string name;
        public AudioClip clip;
        [Range(0, 1)]
        public float volume;
    }
}
