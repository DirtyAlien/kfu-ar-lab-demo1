﻿using System;
using UnityEngine;

public static class Utils {
    public static double Deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double Rad2deg(double rad) {
        return (rad / Math.PI * 180.0);
    }

    public static double GetGeoDistanceInMeters(double fromLat, double fromLon, double toLat, double toLon) {
        double theta = fromLon - toLon;
        double fromLatRad = Deg2rad(fromLat);
        double toLatRad = Deg2rad(toLat);
        double dist = Math.Sin(fromLatRad) * Math.Sin(toLatRad) + Math.Cos(fromLatRad) * Math.Cos(toLatRad) * Math.Cos(Deg2rad(theta));
        dist = Math.Acos(dist);
        dist = Rad2deg(dist);
        return dist * 111189.57696;
    }

    public static Vector2 LatLonToXY(float latitude, float longitude) {
        if (latitude > 89.5) {
            latitude = 89.5f;
        } else if (latitude < -89.5) {
            latitude = -89.5f;
        }

        double rLat = Deg2rad(latitude);
        double rLon = Deg2rad(longitude);

        float a = 6378137f;
        float b = 6356752.3142f;
        float f = (a - b) / a;
        float e = (float)Math.Sqrt(2f * f - Math.Pow(f, 2f));
        float x = (float)(a * rLon);
        float y = (float)(a * Math.Log(Math.Tan(Math.PI / 4 + rLat / 2) * Math.Pow((1 - e * Math.Sin(rLat)) / (1 + e * Math.Sin(rLat)), e / 2f)));
        return new Vector2(x, y);
    }
}
