﻿using UnityEngine;
using UnityEngine.UI;

public class ArticlePanel : AHidablePanel {

    [SerializeField]
    private RectTransform contentHolder;

    [SerializeField]
    private Text titlePrefab;
    [SerializeField]
    private Text textBlockPrefab;
    [SerializeField]
    private Image imageBlockPrefab;
    [SerializeField]
    private RectTransform indentBlockPrefab;
    [SerializeField]
    private RectTransform pageBottomPrefab;

    public void Show(SpotData spotData) {
        LoadSpotData(spotData);
        base.Show();
    }

    public override void OnHideCompleted() {
        for (int i = 0; i < contentHolder.childCount; i++) {
            Destroy(contentHolder.GetChild(i).gameObject);
        }
        contentHolder.anchoredPosition = new Vector2(0, 0);
    }

    public void LoadSpotData(SpotData spotData) {
        var title = Instantiate(titlePrefab, contentHolder);
        title.text = spotData.name;
        foreach (var articleBlockData in spotData.articleBlocks) {
            if (articleBlockData.type == "plane_text") {
                var textBlock = Instantiate(textBlockPrefab, contentHolder);
                textBlock.text = articleBlockData.content;
            } else if (articleBlockData.type == "image_path") {
                var imageBlock = Instantiate(imageBlockPrefab, contentHolder);
                imageBlock.sprite = Resources.Load<Sprite>(articleBlockData.content);
                imageBlock.GetComponent<AspectRatioFitter>().aspectRatio = (float)imageBlock.sprite.texture.width / imageBlock.sprite.texture.height;
            } else if (articleBlockData.type == "text_path") {
                var textBlock = Instantiate(textBlockPrefab, contentHolder);
                textBlock.text = Resources.Load<TextAsset>(articleBlockData.content).text;
            } else if (articleBlockData.type == "space") {
                var indentBlock = Instantiate(indentBlockPrefab, contentHolder);
                indentBlock.sizeDelta = new Vector2(1, int.Parse(articleBlockData.content));
            }
        }
        Instantiate(pageBottomPrefab, contentHolder);
    }
}
