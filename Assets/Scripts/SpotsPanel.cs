﻿using System.Collections.Generic;
using UnityEngine;

public class SpotsPanel : MonoBehaviour {

    [SerializeField]
    private RectTransform contentHolder;
    [SerializeField]
    private SpotItem spotItemPrefab;

    private object hidenSpotDataLock = new object();
    private List<System.Tuple<string, SpotData>> hidenSpotData = new List<System.Tuple<string, SpotData>>();
    private List<SpotItem> showedSpotItems = new List<SpotItem>();

    private void Start () {
        foreach (var item in ContentManager.Instance.ContentData) {
            hidenSpotData.Add(new System.Tuple<string, SpotData>(item.Key, item.Value));
        }
        LocationService.Instance.OnNewLocationInfoAvailable += LocationService_OnNewLocationInfoAvailable;
    }

    private void LocationService_OnNewLocationInfoAvailable(float latitude, float longitude) {
        var dataToRemove = new List<System.Tuple<string, SpotData>>();
        lock (hidenSpotDataLock) {
            foreach (var data in hidenSpotData) {
                if (Utils.GetGeoDistanceInMeters(latitude, longitude, data.Item2.latitude, data.Item2.longitude) <= data.Item2.visibleDistance) {
                    dataToRemove.Add(data);
                    var spotItem = Instantiate(spotItemPrefab, contentHolder);
                    spotItem.FromSpotData(this, data.Item2, data.Item1);
                    showedSpotItems.Add(spotItem);
                }
            }
            foreach (var data in dataToRemove) {
                hidenSpotData.Remove(data);
            }

            foreach (var item in showedSpotItems) {
                item.CheckDistance(latitude, longitude);
            }
        }
    }

    public void RemoveSpotItem(SpotItem spotItem) {
        showedSpotItems.Remove(spotItem);
        lock (hidenSpotDataLock) {
            hidenSpotData.Add(new System.Tuple<string, SpotData>(spotItem.MySpotCodeName, spotItem.MySpotData));
        }
        Destroy(spotItem.gameObject);
    }

    public void RemoveAllItems() {
        foreach (var item in showedSpotItems) {
            item.Remove();
        }
    }
}
