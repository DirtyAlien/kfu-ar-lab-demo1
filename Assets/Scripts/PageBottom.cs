﻿using UnityEngine;

public class PageBottom : MonoBehaviour {

	public void OnBackButtonPressed() {
        GetComponentInParent<ArticlePanel>().Hide();
    }
}
