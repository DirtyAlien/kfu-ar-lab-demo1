﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPanel : AHidablePanel {

    [SerializeField]
    private Vector2 topLeftLatLon;
    [SerializeField]
    private Vector2 bottomRightLatLon;
    [SerializeField]
    private PulseSpawner userPosition;
    [SerializeField]
    private RectTransform spotsHolder;
    [SerializeField]
    private SpotPosition spotPositionPrefab;
    [SerializeField]
    private RectTransform contentHolder;
    [SerializeField]
    private Text researchedText;
    [SerializeField]
    private Text researchedTitul;
    [SerializeField]
    private string[] tituls;

    private Vector2 topLeftXY;
    private Vector2 bottomRightXY;
    private double width;
    private double height;
    private double widthScale;
    private double heightScale;
    private Dictionary<string, SpotPosition> spotPositionsByCodeName = new Dictionary<string, SpotPosition>();
    private Vector2? targetUserCoords = null;
    private Vector2 targetUserCoordsVelocity;
    private int visitedSpotsCount = 0;
    private int totalSpotsCount;

    protected override void Start() {
        base.Start();
        totalSpotsCount = ContentManager.Instance.ContentData.Count;
        topLeftXY = Utils.LatLonToXY(topLeftLatLon.x, topLeftLatLon.y);
        bottomRightXY = Utils.LatLonToXY(bottomRightLatLon.x, bottomRightLatLon.y);
        width = (double)bottomRightXY.x - topLeftXY.x;
        height = (double)topLeftXY.y - bottomRightXY.y;
        widthScale = contentHolder.rect.width / width;
        heightScale = contentHolder.rect.height / height;

        CreateSpotsMarkers();
        UpdateStatistics();

        LocationService.Instance.OnNewLocationInfoAvailable += LocationService_OnNewLocationInfoAvailable;
    }

    protected override void Update() {
        base.Update();
        if (targetUserCoords.HasValue) {
            userPosition.RectTransform.anchoredPosition = Vector2.SmoothDamp(userPosition.RectTransform.anchoredPosition, targetUserCoords.Value, ref targetUserCoordsVelocity, .1f);
        }
    }

    public override void Show() {
        base.Show();
        if (targetUserCoords.HasValue) {
            var userPos = targetUserCoords.Value;
            userPos = new Vector2(userPos.x * contentHolder.localScale.x, -userPos.y * contentHolder.localScale.y);
            contentHolder.anchoredPosition = new Vector2(
                -Mathf.Clamp(userPos.x - rectTransform.rect.width / 2f, 0, 4096 * contentHolder.localScale.x - rectTransform.rect.width),
                Mathf.Clamp(userPos.y - rectTransform.rect.height / 2f, 0, 8192 * contentHolder.localScale.y - rectTransform.rect.height));
        }
    }

    private void LocationService_OnNewLocationInfoAvailable(float latitude, float longitude) {
        var coords = GeoCoordsToMapCoords(latitude, longitude);
        userPosition.gameObject.SetActive(true);
        if (!targetUserCoords.HasValue) {
            userPosition.RectTransform.anchoredPosition = coords;
        }
        targetUserCoords = coords;
    }

    private void CreateSpotsMarkers() {
        foreach (var contentItem in ContentManager.Instance.ContentData) {
            float latitude = contentItem.Value.latitude;
            float longitude = contentItem.Value.longitude;
            var coords = GeoCoordsToMapCoords(latitude, longitude);
            var spotPosition = Instantiate(spotPositionPrefab, spotsHolder);
            (spotPosition.transform as RectTransform).anchoredPosition = coords;
            spotPositionsByCodeName.Add(contentItem.Key, spotPosition);

            if (PlayerPrefs.GetInt("Visited_" + contentItem.Key, 0) == 1) {
                spotPosition.SetVisited();
                visitedSpotsCount++;
                UpdateStatistics();
            }
        }
    }

    public Vector2 GeoCoordsToMapCoords(float latitude, float longitude) {
        Vector2 coords = Utils.LatLonToXY(latitude, longitude);
        Vector2 offset = coords - topLeftXY;
        return new Vector2((float)(offset.x * widthScale), (float)(offset.y * heightScale)); ;
    }

    public void SetSpotPositionVisited(string codeName) {
        if (spotPositionsByCodeName.TryGetValue(codeName, out SpotPosition spotPosition)) {
            if (!spotPosition.IsVisited) {
                PlayerPrefs.SetInt("Visited_" + codeName, 1);
                spotPositionsByCodeName[codeName].SetVisited();
                visitedSpotsCount++;
                UpdateStatistics();
            }
        }
    }

    public void UpdateStatistics() {
        float percent = (float)visitedSpotsCount / totalSpotsCount;
        researchedText.text = visitedSpotsCount + " из " + totalSpotsCount + " (" + Mathf.RoundToInt(percent * 100f) + "%)";
        int level = Mathf.RoundToInt(percent * (tituls.Length - 1));
        researchedTitul.text = "Уровень: " + '"' + tituls[level] + '"';
    }

    public void OnBackButtonPressed() {
        Hide();
    }

    public override void OnHideCompleted() {}
}
