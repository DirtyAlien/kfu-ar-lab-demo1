﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SpotItem : MonoBehaviour {

    public SpotsPanel SpotsManager { get; private set; }
    public string MySpotCodeName { get; private set; }
    public SpotData MySpotData { get; private set; }

    [SerializeField]
    private float showedHeight;
    [SerializeField]
    private float hidedHeight;
    [SerializeField]
    private Text title;
    [SerializeField]
    private Image previewImage;
    [SerializeField]
    private Text description;
    [SerializeField]
    private Button moreButton;
    [SerializeField]
    private Text distance;
    [SerializeField]
    private Button hideShowButton;
    [SerializeField]
    private RectTransform hideButtonIcon;
    [SerializeField]
    private RectTransform showButtonIcon;
    [SerializeField]
    private float maxTooFarDistanceTimeInSec;
    [SerializeField]
    private Image backgroundImage;
    [SerializeField]
    private Color farDistanceColor;
    [SerializeField]
    private Color closeDistanceColor;

    private bool isShowed = false;
    private bool isInAnimation = false;
    private bool canReadArticle = false;
    private RectTransform rectTransform;
    private bool isTooFar = false;
    private float lastTooFarTime;
    private bool isDestroying = false;
    private float closeColorPercent = 0f;
    private float closeColorPercentTarget = 0f;
    private float closeColorPercentVelocity;

    private void Awake() {
        rectTransform = transform as RectTransform; 
    }

    private void Update() {
        closeColorPercent = Mathf.SmoothDamp(closeColorPercent, closeColorPercentTarget, ref closeColorPercentVelocity, .3f);
        backgroundImage.color = Color.Lerp(farDistanceColor, closeDistanceColor, closeColorPercent);
    }

    public void CheckDistance(float latitude, float longitude) {
        if (!isDestroying) {
            float dist = (float)Utils.GetGeoDistanceInMeters(MySpotData.latitude, MySpotData.longitude, latitude, longitude);
            closeColorPercentTarget = 1f - Mathf.Clamp01((dist - MySpotData.requiredDistance) / MySpotData.visibleDistance);
            if (dist < MySpotData.requiredDistance) {
                isTooFar = false;
                distance.text = "Здесь (" + (int)dist + "м)";
                if (isShowed) {
                    if (!isInAnimation && !moreButton.interactable) {
                        moreButton.interactable = true;
                    }
                } else if (!canReadArticle) {
                    Show();
                }
                if (!canReadArticle) {
                    AudioManager.Instance.PlaySound("spot_close");
                }
                canReadArticle = true;
            } else if (dist < MySpotData.visibleDistance) {
                isTooFar = false;
                distance.text = "Рядом (" + (int)dist + "м)\nПодойдите ближе";
                canReadArticle = false;
                moreButton.interactable = false;
            } else if (!isTooFar) {
                canReadArticle = false;
                isTooFar = true;
                lastTooFarTime = Time.realtimeSinceStartup;
            }
            if (isTooFar) {
                distance.text = "Далеко (" + (int)dist + "м)";
                if (Time.realtimeSinceStartup - lastTooFarTime > maxTooFarDistanceTimeInSec) {
                    Remove();
                }
            }
        }
    }

    public void FromSpotData(SpotsPanel spotsManager, SpotData spotData, string spotCodeName) {
        SpotsManager = spotsManager;
        MySpotData = spotData;
        MySpotCodeName = spotCodeName;
        title.text = spotData.name;
        previewImage.sprite = Resources.Load<Sprite>(spotData.previewImage);
        description.text = spotData.description;
        Hide();
    }

    public void OnHideShowButtonPressed() {
        if (!isInAnimation) {
            AudioManager.Instance.PlaySound("click");
            if (isShowed) {
                Hide();
            } else {
                Show();
            }
        }
    }

    public void Show() {
        isShowed = true;
        StopAllCoroutines();
        StartCoroutine(HeightChangeAnimation(.1f, showedHeight, () => {
            description.gameObject.SetActive(true);
            distance.gameObject.SetActive(true);
            if (canReadArticle) {
                moreButton.interactable = true;
            }
        }));
        hideButtonIcon.gameObject.SetActive(true);
        showButtonIcon.gameObject.SetActive(false);
    }

    public void Hide() {
        isShowed = false;
        moreButton.interactable = false;
        description.gameObject.SetActive(false);
        distance.gameObject.SetActive(false);
        StopAllCoroutines();
        StartCoroutine(HeightChangeAnimation(.1f, hidedHeight));
        hideButtonIcon.gameObject.SetActive(false);
        showButtonIcon.gameObject.SetActive(true);
    }

    public void Remove() {
        isDestroying = true;
        moreButton.interactable = false;
        description.gameObject.SetActive(false);
        distance.gameObject.SetActive(false);
        StopAllCoroutines();
        StartCoroutine(HeightChangeAnimation(.1f, 0, () => {
            SpotsManager.RemoveSpotItem(this);
            Destroy(gameObject);
        }));
    }

    public void OnMoreButtomPressed() {
        AudioManager.Instance.PlaySound("article");
        UIManager.Instance.ArticlePanel.Show(MySpotData);
        UIManager.Instance.MapPanel.SetSpotPositionVisited(MySpotCodeName);
    }

    private IEnumerator HeightChangeAnimation(float time, float to, System.Action OnAnimationFinish = null) {
        isInAnimation = true;
        float from = rectTransform.rect.height;
        float speed = 1f / time;
        float percent = 0;
        float width = rectTransform.rect.width;
        bool isAnimationCompelted = false;
        while (!isAnimationCompelted) {
            rectTransform.sizeDelta = new Vector2(width, Mathf.Lerp(from, to, Mathf.Clamp01(percent)));
            bool isTitleVisible = rectTransform.rect.height >= hidedHeight;
            title.gameObject.SetActive(isTitleVisible);
            hideShowButton.gameObject.SetActive(isTitleVisible);
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent as RectTransform);
            if (percent >= 1) {
                isAnimationCompelted = true;
            }
            percent += Time.deltaTime * speed;
            yield return null;
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent as RectTransform);
        isInAnimation = false;
        OnAnimationFinish?.Invoke();
    }
}
