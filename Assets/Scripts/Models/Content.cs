﻿using System.Collections.Generic;

[System.Serializable]
public struct ContentData {

    public Dictionary<string, SpotData> spots;
}