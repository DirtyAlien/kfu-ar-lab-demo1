﻿using System.Collections.Generic;

[System.Serializable]
public struct SpotData {

    public string name;
    public float latitude;
    public float longitude;
    public int requiredDistance;
    public int visibleDistance;
    public string description;
    public string previewImage;
    public List<ArticleBlockData> articleBlocks;
}
