﻿using UnityEngine;
using UnityEngine.UI;

public class SpotPosition : MonoBehaviour {

    public bool IsVisited { get; private set; } = false;

    [SerializeField]
    private Color visitedColor;

    private Image image;
    private PulseSpawner pulseSpawner;

    private void Awake() {
        image = GetComponent<Image>();
        pulseSpawner = GetComponent<PulseSpawner>();
    }

    public void SetVisited() {
        IsVisited = true;
        image.color = visitedColor;
        Destroy(pulseSpawner);
    }
}
