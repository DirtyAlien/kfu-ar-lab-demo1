﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PulseAnimation : MonoBehaviour {

    [SerializeField]
    private float lifetime;
    [SerializeField]
    private Color toColor;
    [SerializeField]
    private Vector2 maxSize;

    private RectTransform rectTransform;
    private Image image;
    private Color startColor;

    private void Start () {
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        startColor = image.color;
        StartCoroutine(RadarAnimation());
    }

    private IEnumerator RadarAnimation() {
        float speed = 1f / lifetime;
        float percent = 0;
        float startW = rectTransform.rect.width;
        float startH = rectTransform.rect.height;
        while (percent < 1) {
            float w = Mathf.Lerp(startW, maxSize.x, percent);
            float h = Mathf.Lerp(startH, maxSize.y, percent);
            rectTransform.sizeDelta = new Vector2(w, h);
            image.color = Color.Lerp(startColor, toColor, percent);
            percent += Time.deltaTime * speed;
            yield return null;
        }
        Destroy(gameObject);
    }
}
